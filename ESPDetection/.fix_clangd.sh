#!/bin/bash
#
sed -i -E 's#-mlongcalls##g' compile_commands.json
sed -i -E 's#-fstrict-volatile-bitfields##g' compile_commands.json
sed -i -E 's#-mfix-esp32-psram-cache-issue##g' compile_commands.json
sed -i -E 's#-fno-tree-switch-conversion##g' compile_commands.json
