#include "EEPROM.h"
#include "EloquentSurveillance.h"
#include "camera.h"
#include "motor.h"
#include "LEDS.h"
#include "driver/rtc_io.h"

#define VERBOSE

const int NUM_FRAMES = 150;

const float MIN_CHANGES = 0.05;
const float PIXEL_DIFF = 7;
const float SIZE_DIFF = 0.01;

#define ESP_DETERRENCE_PIN 14

uint camera_trip = 0;
const int CAMERA_TRIP_ADDR = 8;

EloquentSurveillance::Motion motion;

// EloquentSurveillance::Region region = {
//         .x = 100, // top-left corner x coordinate
//         .y = 100, // top-left corner y coordinate
//         .width = 50,
//         .height = 50,
//         // inherit from motion configuration
//         .numChanges = 0
// };

void trigger_deterrence() {
  Serial.println("Triggering deterrence");

  // Make deterrence pin high
  digitalWrite(ESP_DETERRENCE_PIN, HIGH);
  rtc_gpio_hold_dis((gpio_num_t)ESP_DETERRENCE_PIN);

  delay(1500);

  // Make deterrence pin low
  digitalWrite(ESP_DETERRENCE_PIN, LOW);

  rtc_gpio_hold_en((gpio_num_t)ESP_DETERRENCE_PIN);

  strobe_LEDS();
  start_motor_deterrance();
}

void setup_camera() {
  pinMode(ESP_DETERRENCE_PIN, OUTPUT);

  camera.aithinker();
  camera.svga();
  camera.highQuality();
  // camera.lowQuality();
  camera.disableAutomaticExposureControl();
  camera.disableAutomaticGainControl();
  camera.disableAutomaticWhiteBalance();
  camera.disableAutomaticWhiteBalanceGain();

  motion.setMinChanges(MIN_CHANGES);
  motion.setMinPixelDiff(PIXEL_DIFF);
  // motion.setMinSizeDiff(SIZE_DIFF);

  while (!camera.begin()) {
    debug("ERROR", camera.getErrorMessage());
  }

  debug("SUCCESS", "Camera setup OK");
}

void run_motion_detection(EEPROMClass *EEPROM) {
  setup_camera();

  for (int i = 0; i < NUM_FRAMES; i++) {
    if (!camera.capture()) {
      debug("ERROR", camera.getErrorMessage());
      continue;
    }

    if (!motion.update()) {
      debug("INFO", "No motion");
      continue;
    }

    if (motion.detect()) {
      debug("INFO", "Motion detected by camera");

      camera_trip = EEPROM->readUInt(CAMERA_TRIP_ADDR);
      camera_trip++;
      EEPROM->writeUInt(CAMERA_TRIP_ADDR, camera_trip);

      Serial.println("Camera trip count: " + String(camera_trip));

      trigger_deterrence();

      return;
    }
  }
}
