// #include "camera.h"
#include "driver/rtc_io.h"
#include "hal/gpio_types.h"
#include "soc/rtc_cntl_reg.h" // Disable brownour problems
#include "soc/rtc_wdt.h"
#include "soc/soc.h" // Disable brownour problems
#include "util.h"
#include <Arduino.h>
#include <cmath>
#include <EEPROM.h>
#include "motion/CameraWifiMotion.h"
#include "camera.h"

const int PIR_PIN1 = 12;
const int PIR_PIN2 = 15;

#define PIR_PIN_BITMASK 0x9000 // pins 12 & 13 for PIR wakeup

bool checkPIRsHigh() {
  return (digitalRead(PIR_PIN1) & digitalRead(PIR_PIN2));
}

const int SERIAL_BAUD = 115200;
const int START_DELAY = 3000;

#define BUTTON_PIN_BITMASK 0x2000
#define FLASH_PIN 4
#define FLASH_ENABLED false

#define EEPROM_SIZE 32

uint boot_count = 0;
uint pir_trip = 0;

const int BOOT_COUNT_ADDR = 0;
const int PIR_TRIP_ADDR = 4;

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); // disable brownout detector
  // Turn off problematic watchdog
  rtc_wdt_protect_off();
  rtc_wdt_disable();

  // Turn camera flash on
  pinMode(FLASH_PIN, OUTPUT);
  digitalWrite(FLASH_PIN, LOW);

  pinMode(14, OUTPUT);
  digitalWrite(14, LOW);

  EEPROM.begin(EEPROM_SIZE);

  if (FLASH_ENABLED) {
    digitalWrite(FLASH_PIN, HIGH);
    rtc_gpio_hold_dis((gpio_num_t)FLASH_PIN);
  }

  // Start serial
  Serial.begin(SERIAL_BAUD);
  Serial.setDebugOutput(true);
  delay(START_DELAY);

  // Determine wake up pin
  uint64_t GPIO_reason = esp_sleep_get_ext1_wakeup_status();
  Serial.print("GPIO that triggered the wake up: GPIO ");
  Serial.println((log(GPIO_reason))/log(2), 0);

  // Increment bootcount and print it
  boot_count = EEPROM.readUInt(BOOT_COUNT_ADDR);
  boot_count++;
  EEPROM.writeUInt(BOOT_COUNT_ADDR, boot_count);

  Serial.println("starting serial, boot number: " + String(boot_count));

  print_wakeup_reason();

  // Configure wakeup from pir sensor:
  esp_sleep_enable_ext1_wakeup(PIR_PIN_BITMASK, ESP_EXT1_WAKEUP_ANY_HIGH);
  // esp_sleep_enable_ext0_wakeup((gpio_num_t)PIR_PIN, HIGH);

  if(checkPIRsHigh()) {
    Serial.println("Both PIR sensors are high!");

    pir_trip = EEPROM.readUInt(PIR_TRIP_ADDR);
    pir_trip++;
    EEPROM.writeUInt(PIR_TRIP_ADDR, pir_trip);

    Serial.println("PIR trip count: " + String(pir_trip));

    // run_motion_detection(&EEPROM);
    begin_motion_detection();
  }

  delay(500);

  Serial.println("Going to DEEP sleep");

  // Shutoff camera flash before sleeping
  digitalWrite(FLASH_PIN, LOW);
  rtc_gpio_hold_en((gpio_num_t)FLASH_PIN);

  digitalWrite(14, LOW);
  rtc_gpio_hold_en((gpio_num_t)14);

  EEPROM.commit();

  esp_deep_sleep_start();
}

void loop() {
}
