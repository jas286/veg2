#include <Arduino.h>
#include "motor.h"
#include <rom/ets_sys.h>
#include <ESP32_Servo.h> 

Servo myServo;

#define SERVO_PIN 2
#define MOTOR_DELAY 300

void motor_loop() {
    myServo.write(180);
    delay(MOTOR_DELAY);
    myServo.write(0);
    delay(MOTOR_DELAY);
}

void start_motor_deterrance() {
    myServo.attach(SERVO_PIN);
    Serial.println("Starting motor deterrance");
    for (int i = 0; i < 6; i++) {
        motor_loop();
    }
}

