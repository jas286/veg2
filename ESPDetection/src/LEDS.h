#pragma once

#include <Adafruit_NeoPixel.h>
#define LED_PIN   13
#define NUMPIXELS 5

Adafruit_NeoPixel pixels(NUMPIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 200

int BOOTS = 0;

void set_random_LED_color(int idx)
{
  srand(time(0));
  int rgb[3];
  for(int i=0;i<3;i++)
  {
    rgb[i]=rand()%256;
  }
  pixels.setPixelColor(idx, pixels.Color(rgb[0], rgb[1], rgb[2]));
}

void lightup_LEDS() {
  for (int i = 0; i < NUMPIXELS; ++i) {
    set_random_LED_color(i);
  }
}

void strobe_LEDS() {
  pixels.begin();
  Serial.println("Strobing the lights!");
  for (int i = 0; i < 10; i++) {
    if (i % 2 == 0) {
      lightup_LEDS();
    }
    else {
      pixels.clear();
    }
    pixels.show();
    delay(DELAYVAL);
  }
  pixels.clear();
  pixels.show();
}
