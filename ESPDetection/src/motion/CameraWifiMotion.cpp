#if (!defined ESP32)
  #error This sketch is for esp32cam only
#endif

#include <Arduino.h>    // required by PlatformIO


// ---------------------------------------------------------------
//                       - S E T T I N G S -
// ---------------------------------------------------------------


  const char* stitle = "CameraWifiMotion";               // title of this sketch (CameraWifiMotion)

  const char* sversion = "13Jan23";                      // version of this sketch

  const bool serialDebug = 0;                            // provide debug info on serial port

  bool flashIndicatorLED = 1;                            // flash the onboard led when detection is enabled

  #define EMAIL_ENABLED 1                                // Enable E-mail support

  #define ENABLE_OTA 1                                   // Enable Over The Air updates (OTA)
  const String OTAPassword = "password";                 // Password to enable OTA service (supplied as - http://<ip address>?pwd=xxxx )

  #define FTP_ENABLED 0                                  // if ftp uploads are enabled

  #define PHP_ENABLED 1                                  // if PHP uploads are enabled

  const String HomeLink = "/";                           // Where home button on web pages links to (usually "/")

  const byte LogNumber = 30;                             // number of entries in the system log

  const uint16_t ServerPort = 80;                        // ip port to serve web pages on

  const byte onboardLED = 33;                            // indicator LED

  const bool ledBlinkEnabled = 1;                        // enable blinking onboard status LED
  const uint16_t ledBlinkRate = 1500;                    // Speed to blink the status LED (milliseconds) - also perform some system tasks

  const int serialSpeed = 115200;                        // Serial data speed to use

  const uint16_t MaintCheckRate = 5;                     // how often to carry out routine system checks (seconds)


  // camera related

  #define IMAGE_SETTINGS 1                               // Implement adjustment of camera sensor settings

  const uint32_t maxCamStreamTime = 60;                  // max camera stream can run for (seconds)

  const uint16_t Illumination_led = 4;                   // illumination LED pin

  const byte flashMode = 2;                              // 1=take picture using flash when dark, 2=use flash every time, 3=flash after capturing the image as display only

  const byte gioPin = 13;                                // I/O pin (for external sensor input)

  bool ioRequiredHighToTrigger = 0;                      // If motion detection only triggers if IO input is also high

  int8_t cameraImageBrightness = 0;                      // image brighness (-2 to 2) - Note: has no effect?

  int8_t cameraImageContrast = 0;                        // image contrast (-2 to 2) - Note: has no effect?

  float thresholdGainCompensation = 0.65;                // motion detection level compensation for increased noise in image when gain increased (i.e. in darker conditions)

  // Note: to adjust other camera sensor settings see 'cameraImageSettings()' in 'motion.h'


// ---------------------------------------------------------------


#include "esp_camera.h"       // https://github.com/espressif/esp32-camera


// forward declarations - many of these are only required by platformio
  void log_system_message(String smes);          // in standard.h
  void BlinkLed(byte Bcount);
  void AutoAdjustImage();
  void RestartCamera(pixformat_t format);
  void RebootCamera(pixformat_t format);
  void ioDetected(bool iostat);
  void MotionDetected(uint16_t changes);
  bool checkCameraIsFree();


// ---------------------------------------------------------------


// global variables / constants
  bool wifiok = 0;                           // Flag if wifi is connected ok
  uint16_t dataRefresh = 5;                  // How often the updating info. on root page refreshes (seconds)
  bool cameraImageInvert = 0;                // flip image  (i.e. upside down)
  float cameraImageExposure = 0;             // Camera exposure (loaded from spiffs)
  float cameraImageGain = 0;                 // Image gain (loaded from spiffs)
  uint32_t TRIGGERtimer = 0;                 // used for limiting camera motion trigger rate
  uint32_t EMAILtimer = 0;                   // used for limiting rate emails can be sent
  byte DetectionEnabled = 1;                 // flag if motion detection is enabled (0=stopped, 1=enabled, 2=paused)
  String TriggerTime = "Not yet triggered";  // Time of last motion detection as text
  uint32_t MaintTiming = millis();           // used for timing maintenance tasks
  bool emailWhenTriggered = 0;               // If emails will be sent when motion detected
  bool ftpImages = 0;                        // if to FTP images up to server (ftp.h)
  bool PHPImages = 0;                        // if to send images via PHP script (php.h)
  bool ReqLEDStatus = 0;                     // desired status of the illuminator led (i.e. should it be on or off when not being used as a flash)
  const bool ledON = HIGH;                   // Status LED control
  const bool ledOFF = LOW;
  uint16_t TriggerLimitTime = 2;             // min time between motion detection trigger events (seconds)
  uint16_t EmailLimitTime = 60;              // min time between email sending (seconds)
  bool UseFlash = 1;                         // use flash when taking a picture (1=yes)
  bool SensorStatus = 1;                     // Status of the sensor i/o pin (gioPin)
  bool OTAEnabled = 0;                       // flag if OTA has been enabled (via supply of password)
  bool disableAllFunctions = 0;              // if set all functions other than web server are disabled


#include "motion.h"                          // Include motion.h file for camera/motion detection code
#include "testing_wifi.h"                            // Load the Wifi / NTP stuff
#include "standard.h"                        // Some standard procedures

// sd card - see https://randomnerdtutorials.com/esp32-cam-take-photo-save-microsd-card/
  #include "SD_MMC.h"
  // #include <SPI.h>                        // (already loaded)
  #include <FS.h>                            // gives file access (already loaded?)
  #define SD_CS 5                            // sd chip select pin
  bool SD_Present;                           // flag if an sd card was found (0 = no)

Led statusLed1(onboardLED, LOW);             // set up onboard LED (LOW = on) - standard.h

#if ENABLE_OTA
  #include "ota.h"                           // Over The Air updates (OTA)
#endif

#if EMAIL_ENABLED
    #define _SenderName "ESP"                // name of email sender (no spaces)
    #include "email.h"
#endif

#if FTP_ENABLED
  #include "ftp.h"                           // Include ftp.h file for the ftp of captured images
#endif

#if PHP_ENABLED
  #include "php.h"                           // Include php.h file for sending images via a php script
#endif

#include "CameraWifiMotion.h"
#include "camera.h"

// ---------------------------------------------------------------
//                -check if camera is already in use
// ---------------------------------------------------------------
// try to avoid two processes capturing an image at the same time by using rudimentary flag
bool checkCameraIsFree() {
  int waitTimeout = 10;         // how long to wait if camera is in use
  if (DetectionEnabled != 2) return 1;
  log_system_message("Waiting to capture image as camera is already in use!");
  for (int i = 0; i < waitTimeout; i++) {
    delay(100);   if (DetectionEnabled != 2) return 1;
  }
  log_system_message("Waiting for camera to become free timed out");
  return 0;
}


// ---------------------------------------------------------------
//    -SETUP     SETUP     SETUP     SETUP     SETUP     SETUP
// ---------------------------------------------------------------
// setup section (runs once at startup)

void camera_motion_setup() {

  // status info. on serial port config
  if (serialDebug) {
    Serial.begin(115200);                              // serial port speed
    Serial.println("\n\n\n");                          // line feeds
    Serial.println("---------------------------------------");
    Serial.printf("Starting - %s - %s \n", stitle, sversion);
    Serial.println("---------------------------------------");
    // Serial.setDebugOutput(true);                       // enable extra diagnostic info on serial port
  }

  // configure the flash/illumination LED
      pinMode(Illumination_led, OUTPUT);
      digitalWrite(Illumination_led, ledOFF);

  BlinkLed(1);           // flash the led once

  // configure the gpio pin (with pullup resistor)
    pinMode(gioPin,  INPUT);
    SensorStatus = 1;

  // configure ondoard indicator led
    pinMode(onboardLED, OUTPUT);
    digitalWrite(onboardLED, HIGH);   // off

  // set up camera
    bool tRes = setupCameraHardware();
    if (!tRes) {      // reboot camera
      if (serialDebug) Serial.println("Problem starting camera - rebooting it");
      RestartCamera(PIXFORMAT_GRAYSCALE);                    // restart camera back to greyscale mode for motion detection
      cameraImageSettings(FRAME_SIZE_MOTION);                // apply camera sensor settings
    } else {
      if (serialDebug) Serial.print(("Camera initialised ok"));
    }

  log_system_message("Setup complete");
}


// blink the led
void BlinkLed(byte Bcount) {
    for (int i = 0; i < Bcount; i++) {
      digitalWrite(Illumination_led, ledON);
      delay(50);
      digitalWrite(Illumination_led, ledOFF);
      delay(300);
    }
}


// ----------------------------------------------------------------
//   -LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP
// ----------------------------------------------------------------

void begin_motion_detection() {
    camera_motion_setup();
    Serial.println("Starting motion detection");
    for (int i = 0; i < 30; i++) {
      if (camera_motion_work()) {
        trigger_deterrence();
        break;
      }
    }
    Serial.println("Ending motion detection");
}   // loop


bool camera_motion_work() {
  // camera motion detection
    if (DetectionEnabled == 1) {
      if (!capture_still()) RebootCamera(PIXFORMAT_GRAYSCALE);                                // capture image, if problem reboot camera and try again
        uint16_t changes = motion_detect();                                                   // find amount of change in current image frame compared to the last one
        update_frame();                                                                       // Copy current stored frame to previous stored frame
        if ( (changes >= Image_thresholdL) && (changes <= Image_thresholdH) ) {               // if enough change to count as motion detected
          if (tCounter >= tCounterTrigger) {                                                  // only trigger if movement detected in more than one consequitive frames
             tCounter = 0;
             if ((unsigned long)(millis() - TRIGGERtimer) >= (TriggerLimitTime * 1000) ) {    // limit time between triggers
                TRIGGERtimer = millis();                                                      // update last trigger time
                // run motion detected procedure (blocked if io high is required)
                  if (ioRequiredHighToTrigger == 0 || SensorStatus == 1) {
                    MotionDetected(changes);
                    return true;
                  } else {
                    log_system_message("Motion detected but io input low so ignored");
                  }
             } else {
               if (serialDebug) {
                 Serial.println("Too soon to re-trigger");
               }
             }
          } else {
            if (serialDebug) {
              Serial.println("Not enough consecutive detections");
            }
          }
       }
    }

  // log when sensor i/o input pin status changes
    bool tstatus = digitalRead(gioPin);
    if (tstatus != SensorStatus) {
      delay(20);
      tstatus = digitalRead(gioPin);        // debounce input
      if (tstatus != SensorStatus) {
        // input pin status has changed
        if (tstatus == 1) {
          SensorStatus = 1;
          ioDetected(1);                    // trigger io status has changed procedure
        } else {
          SensorStatus = 0;
          ioDetected(0);                    // trigger io status has changed procedure
        }
      }
    }

  // periodic system tasks
    if ((unsigned long)(millis() - MaintTiming) >= (MaintCheckRate * 1000) ) {
      if (DetectionEnabled && flashIndicatorLED) digitalWrite(onboardLED, !digitalRead(onboardLED));  // flash onboard indicator led
      WIFIcheck();                                        // check if wifi connection is ok
      MaintTiming = millis();                             // reset system tasks timer
      time_t t=now();                                     // read current time to ensure NTP auto refresh keeps triggering (otherwise only triggers when time is required causing a delay in response)
      // check status of illumination led is correct
        if (ReqLEDStatus) {
          digitalWrite(Illumination_led, ledON);
        } else {
          digitalWrite(Illumination_led, ledOFF);
        }
      if (DetectionEnabled == 0) capture_still();         // capture a frame to get a current brightness reading
      if (targetBrightness > 0) AutoAdjustImage();        // auto adjust image sensor settings
    }

    return false;
}


// Auto image adjustment
//   runs every few seconds, called from loop
void AutoAdjustImage() {
          float exposureAdjustmentSteps = (cameraImageExposure / 25) + 0.2;    // adjust by higher amount when at higher level (25 / 0.2 = default)
          float gainAdjustmentSteps = 0.5;
          float hyster = 20.0;                                                 // Hysteresis on brightness level
          if (AveragePix > (targetBrightness + hyster)) {
            // too bright
            if (cameraImageGain > 0) {
              cameraImageGain -= gainAdjustmentSteps;
            } else {
              cameraImageExposure -= exposureAdjustmentSteps;
            }
          }
          if (AveragePix < (targetBrightness - hyster)) {    // if too dark
            if (cameraImageExposure >= 1200) {
              cameraImageGain += gainAdjustmentSteps;
            } else {
              cameraImageExposure += exposureAdjustmentSteps;
            }
          }
          // check for over scale
            if (cameraImageExposure < 0) cameraImageExposure = 0;
            if (cameraImageExposure > 1200) cameraImageExposure = 1200;
            if (cameraImageGain < 0) cameraImageGain = 0;
            if (cameraImageGain > 30) cameraImageGain = 30;
          cameraImageSettings(FRAME_SIZE_MOTION);      // apply camera sensor settings
          capture_still();                             // update stored image with the changed image settings to prevent trigger
          update_frame();


}  // autoadjustimage

// ----------------------------------------------------------------
//              -restart the camera in different mode
// ----------------------------------------------------------------
// switches camera mode - format = PIXFORMAT_GRAYSCALE or PIXFORMAT_JPEG

void RestartCamera(pixformat_t format) {

    esp_camera_deinit();
    if (format == PIXFORMAT_JPEG) config.frame_size = FRAME_SIZE_PHOTO;
    else if (format == PIXFORMAT_GRAYSCALE) config.frame_size = FRAME_SIZE_MOTION;
    else {
      if (serialDebug) Serial.println("ERROR: Invalid image format");
    }
    config.pixel_format = format;
    bool ok = esp_camera_init(&config);
    if (ok == ESP_OK) {
      if (serialDebug) Serial.println("Camera mode switched ok");
    } else {
      // failed so try again
        esp_camera_deinit();
        delay(50);
        ok = esp_camera_init(&config);
        if (ok == ESP_OK) {
          if (serialDebug) Serial.println("Camera mode switched ok - 2nd attempt");
        } else {
          RebootCamera(format);
        }
    }
    TRIGGERtimer = millis();        // reset last image captured timer (to prevent instant trigger)
}


// reboot camera (used if camera is failing to respond)
//      restart camera in motion mode, capture a test frame to check it is now responding ok
//      format = PIXFORMAT_GRAYSCALE or PIXFORMAT_JPEG

void RebootCamera(pixformat_t format) {

    log_system_message("ERROR: Problem with camera detected so resetting it");
    // turn camera off then back on
      digitalWrite(PWDN_GPIO_NUM, HIGH);
      delay(200);
      digitalWrite(PWDN_GPIO_NUM, LOW);
      delay(400);
    RestartCamera(PIXFORMAT_GRAYSCALE);    // restart camera in motion mode
    delay(50);
    // try capturing a frame, if still problem reboot esp32
      if (!capture_still()) {
          delay(500);
          ESP.restart();
          delay(5000);      // restart will fail without this delay
        }
    if (format == PIXFORMAT_JPEG) RestartCamera(PIXFORMAT_JPEG);                  // if jpg mode required restart camera again
}


// ----------------------------------------------------------------
//                       -gpio input has triggered
// ----------------------------------------------------------------

void ioDetected(bool iostat) {

  checkCameraIsFree();                                                   // try to avoid using camera if already in use
  if (DetectionEnabled == 1) DetectionEnabled = 2;                       // pause motion detecting (not required with single core esp32?)

    log_system_message("IO input has triggered - status = " + String(iostat));

    // TRIGGERtimer = millis();                                             // reset retrigger timer to stop instant motion trigger

  if (DetectionEnabled == 2) DetectionEnabled = 1;                       // restart paused motion detecting
}


// ----------------------------------------------------------------
//                       -motion has been detected
// ----------------------------------------------------------------

void MotionDetected(uint16_t changes) {

  checkCameraIsFree();                                                    // try to avoid using camera if already in use
  if (DetectionEnabled == 1) DetectionEnabled = 2;                        // pause motion detecting (not required with single core esp32?)
    Serial.println("MOTION DETECTED!");
    log_system_message("Camera detected motion: " + String(changes));
    TriggerTime = currentTime(1) + " - " + String(changes) + " out of " + String(mask_active * blocksPerMaskUnit);    // store time of trigger and motion detected
    BlinkLed(1);                        // capture an image

  TRIGGERtimer = millis();                                       // reset retrigger timer to stop instant motion trigger
  if (DetectionEnabled == 2) DetectionEnabled = 1;               // restart paused motion detecting
}



// --------------------------- E N D -----------------------------
