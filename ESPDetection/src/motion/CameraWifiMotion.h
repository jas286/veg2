#pragma once

// forward declarations - many of these are only required by platformio
void camera_motion_setup();
void begin_motion_detection();
bool camera_motion_work();