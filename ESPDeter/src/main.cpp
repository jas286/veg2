#include <Arduino.h>
#include "audio.h"
#include "motor.h"

const int SERIAL_BAUD = 115200;
const int START_DELAY = 3000;

void setup() {
  Serial.begin(SERIAL_BAUD);
  Serial.setDebugOutput(true);
  delay(START_DELAY);
  Serial.println("Starting deterrence");

  run_audio_deterrence();
  // start_motor_deterrance();

  Serial.println("Going to DEEP sleep");

  esp_sleep_enable_ext0_wakeup(GPIO_NUM_14, HIGH);

  esp_deep_sleep_start();

}

void loop() {}
