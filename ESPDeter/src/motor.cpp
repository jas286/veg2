#include <Arduino.h>
#include "motor.h"
#include <rom/ets_sys.h>
#include <ESP32_Servo.h> 

Servo myServo;

#define SERVO_PIN 32

void start_motor_deterrance() {
    myServo.attach(SERVO_PIN);
    Serial.println("starting motor deterrance for 10 seconds");
    myServo.write(0);
    delay(10000);
}