#include "AudioFileSourcePROGMEM.h"
#include "AudioGeneratorAAC.h"
#include "AudioOutputI2S.h"
#include "dog3.h"
#include "bark.h"
#include "howl.h"
#include "dog2.h"
#include "dog1.h"
#include "dog4.h"
#include "dog5.h"
#include "cougar1.h"
#include "hawk1.h"
#include "sampleaac.h"

const int BCLK_PIN = 27;
const int WCLK_PIN = 33;
const int DOUT_PIN = 12;

const float GAIN = 0.5;

AudioFileSourcePROGMEM *in;
AudioGeneratorAAC *aac;
AudioOutputI2S *out;

void setup_audio(float gain) {
  Serial.println("Setting up audio i2s");
  long num = random(0, 10);

  if (num == 0) {
    in = new AudioFileSourcePROGMEM(hawk1, sizeof(hawk1));
  } else if (num == 1){
    in = new AudioFileSourcePROGMEM(dog3, sizeof(dog3));
  } else if (num == 2){
    in = new AudioFileSourcePROGMEM(howl, sizeof(howl));
  } else if (num == 3){
    in = new AudioFileSourcePROGMEM(bark, sizeof(bark));
  } else if (num == 4){
    in = new AudioFileSourcePROGMEM(hawk1, sizeof(hawk1));
  } else if (num == 5){
    in = new AudioFileSourcePROGMEM(dog2, sizeof(dog2));
  } else if (num == 6){
    in = new AudioFileSourcePROGMEM(dog1, sizeof(dog1));
  } else if (num == 7){
    in = new AudioFileSourcePROGMEM(cougar1, sizeof(cougar1));
  } else if (num == 8){
    in = new AudioFileSourcePROGMEM(dog4, sizeof(dog4));
  } else if (num == 9){
    in = new AudioFileSourcePROGMEM(dog5, sizeof(dog5));
  }

  aac = new AudioGeneratorAAC();

  out = new AudioOutputI2S();
  out->SetGain(gain);

  out->SetPinout(BCLK_PIN, WCLK_PIN, DOUT_PIN);

  aac->begin(in, out);
  Serial.println("audio deterrence started");
}

void run_audio_deterrence() {
  setup_audio(GAIN);

  while (aac->isRunning()) {
    aac->loop();
    // yield();
  }

  Serial.println("audio deterrence finished");
  aac->stop();

  delete(in);
  delete(out);
  delete(aac);

}
