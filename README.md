# Solar Powered Pest Deterrent System -- Veg 2

This repo contains the code for Veg2's farm animal deterrent system.
There are two PlatformIO projects, one for each of the two ESP32 microcontrollers.
ESPDetection is used by the ESP32CAM, and signals the secondary ESP32, running ESPDeter, when both PIRs and Camera motion are detected.
This secondary microcontroller uses I2S to drive the speaker subsystem to deter the pest.
